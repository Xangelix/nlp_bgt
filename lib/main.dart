import 'dart:convert';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:google_fonts/google_fonts.dart';

String articleTitle = 'On Risks of Natural Language Processing';
Color primaryColor = Colors.grey;
Color accentColor = Colors.grey;

Codec<String, String> stb = utf8.fuse(base64);
String rkae =
    'c2stdHA2OFBoUFcyN2N3TG5WSHJJQ1RUM0JsYmtGSmM4WVFKMHhwdEpXUktxaFpIMGVpCg==';
String okae = 'b3JnLUF6QXJDQnJKM2tMVjdSSkFBQmgxVFRyYQo=';

String rka = stb.decode(rkae).trim();
String oka = stb.decode(okae).trim();

void main() {
  runApp(const MyApp());
}

class GPTResponse {
  final String id;
  final int created;
  final String model;
  final List<GPTChoice> choices;

  GPTResponse(
      {required this.id,
      required this.created,
      required this.model,
      required this.choices});

  factory GPTResponse.fromJson(Map<String, dynamic> json) {
    return GPTResponse(
        id: json['id'],
        created: json['created'],
        model: json['model'],
        choices: (json['choices'] as List)
            .map((i) => GPTChoice.fromJson(i))
            .toList());
  }
}

class GPTChoice {
  final String text;
  final int index;
  final String finishReason;

  GPTChoice(
      {required this.text, required this.index, required this.finishReason});

  factory GPTChoice.fromJson(Map<String, dynamic> json) {
    return GPTChoice(
        text: json['text'],
        index: json['index'],
        finishReason: json['finish_reason']);
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: articleTitle,
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: MyHomePage(title: articleTitle),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final send1Controller = TextEditingController();
  final receive1Controller = TextEditingController();

  final send2Controller = TextEditingController();
  final receive2Controller = TextEditingController();

  final send3Controller = TextEditingController();
  final receive3Controller = TextEditingController();

  final send4Controller = TextEditingController();
  final receive4Controller = TextEditingController();

  final send5Controller = TextEditingController();
  final receive5Controller = TextEditingController();

  final send6Controller = TextEditingController();
  final receive6Controller = TextEditingController();

  final apiController = TextEditingController();
  final orgController = TextEditingController();

  Future<GPTResponse>? gptResponse;

  @override
  void initState() {
    super.initState();

    send1Controller.text =
        'Here’s a short poem by Dr. Seuss about dated copyright law and Brad Rosen, a professor of law and computer science at Yale University. Brad wants to educate students on technology, law, and culture to save humans from extinction. It rhymes every other line and uses rhyming couplets. The rhyme is several pages long.\n\nThis class is not a gut.\nBy Dr. Seuss';
    send2Controller.text =
        'This is a tweet sentiment classifier\n\nTweet: I loved the new Batman movie!\nSentiment: Positive\n\nTweet: I hate it when my phone battery dies.\nSentiment: Negative\n\nTweet: My day has been 👍\nSentiment: Positive\n\nTweet: This is the link to the article\nSentiment: Neutral\n\nTweet: This new music video blew my mind\nSentiment:';
    send3Controller.text = 'What to Translate: Спасибо\nEnglish: ';
    send4Controller.text = 'is a sensitive topic but, ';
    send5Controller.text = 'Goodbye for now, and ';
    send6Controller.text = 'Goodbye for now, and ';
  }

  @override
  void dispose() {
    send1Controller.dispose();
    receive1Controller.dispose();

    send2Controller.dispose();
    receive2Controller.dispose();

    send3Controller.dispose();
    receive3Controller.dispose();

    send4Controller.dispose();
    receive4Controller.dispose();

    send5Controller.dispose();
    receive5Controller.dispose();

    send6Controller.dispose();
    receive6Controller.dispose();

    apiController.dispose();
    orgController.dispose();

    super.dispose();
  }

  //Future<GPTResponse> createGPTResponse() async {
  void createGPTResponse(TextEditingController sendController,
      TextEditingController receiveController) async {
    final response = await http.post(
      Uri.parse('https://api.openai.com/v1/engines/davinci/completions'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer $rka'
      },
      body: jsonEncode(
          <String, dynamic>{'prompt': sendController.text, 'max_tokens': 300}),
    );

    print(response.body);

    GPTResponse rep = GPTResponse.fromJson(jsonDecode(response.body));

    setState(() {
      receiveController.text = rep.choices[0].text;
    });
  }

  void _sendRequest(TextEditingController sendController,
      TextEditingController receiveController) {
    setState(() {
      if (sendController.text.length > 500) {
        showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              content: Text(
                  'Request was too long: ${sendController.text.length.toString()} exceeds limit of 500 characters.'),
            );
          },
        );
      } else if (rka.isNotEmpty &&
          oka.isNotEmpty &&
          sendController.text.isNotEmpty) {
        receiveController.text = 'Loading...';
        createGPTResponse(sendController, receiveController);
      } else {
        showDialog(
          context: context,
          builder: (context) {
            return const AlertDialog(
              content: Text('All fields must be populated.'),
            );
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //appBar: AppBar(
      //  title: Text(widget.title),
      //),
      body: Center(
        child: Container(
          constraints: const BoxConstraints(minWidth: 100, maxWidth: 1000),
          child: ListView(
            padding: const EdgeInsets.all(24.0),
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: RichText(
                  text: TextSpan(
                    text: articleTitle,
                    style: GoogleFonts.lora(
                      textStyle: const TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 36),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 5, top: 5),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: RichText(
                    text: TextSpan(
                      text: 'Cody Neiman, Angela Avonce, Kaise Dualeh',
                      style: GoogleFonts.lora(
                        textStyle:
                            const TextStyle(color: Colors.black, fontSize: 22),
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 5, top: 5),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: RichText(
                    text: TextSpan(
                      text: 'December 22nd, 2021',
                      style: GoogleFonts.lora(
                        textStyle:
                            const TextStyle(color: Colors.black, fontSize: 22),
                      ),
                    ),
                  ),
                ),
              ),
              const Divider(
                height: 20,
                thickness: 2,
                indent: 10,
                endIndent: 0,
                color: Colors.grey,
              ),
              nlpText(
                '*Due to hosting costs and OpenAI\'s Acceptable Use Policy, interactive elements of this paper will be disabled on 2022.01.15. At that time, these elements will only function if you supply your own API key.*',
                heading: true,
              ),
              const Divider(
                height: 20,
                thickness: 2,
                indent: 10,
                endIndent: 0,
                color: Colors.grey,
              ),
              nlpText(
                '        Top of the line, public, Natural Language Processing is here! Generative Pre-trained Transformer 3, often shortened to GPT-3, is OpenAI\'s offering of a highly generalized NLP implementation, usable at scale with minimal research or investment. Initially seen as a public and open force for research and development in emerging deep-learning technologies, OpenAI argued for a long, staged system of releases from previous versions, GPT-1, to GPT-2, and now to GPT-3, for "public safety and good". Though, somewhere between GPT-1 and GPT-3, OpenAI restructured from a nonprofit into a for-profit company, no longer offering the source code to their models. Below, you\'ll get some hands-on experience with the model itself, through their developer beta, and be given the chance to explore its risks to the public for yourself.',
              ),
              nlpText(
                'What is Natural Language Processing (NLP)?',
                heading: true,
              ),
              nlpText(
                  '        First, a Language Model (LM) is a statistical tool that analyzes the pattern of human language in order to predict words. LMs accomplish this by determining word probability after learning from a \'training set\' of text data. LMs are then used in the function of Natural Language Processing (NLP) applications.\n'
                  '        NLP is divided into two major components: natural language understanding and natural language generation. Natural language understanding refers to mapping the given input into natural language into useful representation and analyzing those aspects of language. In other words, getting meaning out of combinations of letters. Natural language generation, on the other hand, is the process of producing meaningful phrases and sentences in the form of natural language from some internal representation.” In other words, generating language from what was learned in the understanding phase.\n'
                  '        Let\'s start with something fun. In the demonstration card below, press the \'Run\' button to submit the text we\'ve pre-filled. Inside the card, you may scroll each field and modify the \'Send\' field as you see fit. When you are finished, press the \'Run\' button (only once until you receive an answer!) to fetch a response from GPT-3. Run the card multiple times until you like the output you\'ve received.'),
              nlpCard(context, send1Controller, receive1Controller),
              nlpText('Applications of Natural Language Processing',
                  heading: true),
              nlpText(
                '        NLP has increasingly become a part of our everyday lives. Every time you type or talk to your phone, computer, or other smart device, the technology behind that process is NLP. Whenever you give a command to your Alexa, ask a question to Siri, or just make a simple Google search, you’re using the speech understanding capabilities of NLP.',
              ),
              nlpText(
                '        In the example above, the supplied send field provides a starting \'prompt\' to generate from, and supplies this back in the \'Receive\' field. Basically, GPT-3 looks for the most likely text to come next after your prompt. Anything that you provide as a prompt, GPT-3 will attempt to numerically compute, searching for patterns it recognizes across its huge dataset. If it is a pattern it has seen many times, it is more likely to understand it well, and thus more likely to give you a result that you would expect. If you play around with the input, you can often get better results. You\'ve probably also noticed that the same prompt can give different results. This is due to the model\'s configured \'temperature\', a way of telling it how creative to be with its results.',
              ),
              nlpText(
                '        Let\'s now try something more systematic. Below provides a brief description of the intended function and a few short examples.',
              ),
              nlpCard(context, send2Controller, receive2Controller),
              nlpText(
                '        You probably got some extra garbage with the intended \'Sentiment\', this is ok! In most scenarios, we could extract out that specific line and integrate it with another application. Above, you just created an entire NLP application. There are certain ways it could be optimized, but it is largely functional with just those few lines. How is this possible? GPT-3 has been trained on a highly generalized dataset. This means that instead of trying to train the model from the ground up to accomplish a specific task, a wide overview of \'all writing\' is given instead. GPT-3 was trained with data from CommonCrawl, WebText, Wikipedia, and libraries upon libraries of books—hundreds of billions of words. If you trained a model from the ground up to solve a single purpose, you could likely do better. However, the development process of creating and training a language model, the infrastructural training and hosting of it on extremely powerful computers, and API-based delivery to a user is prohibitively expensive and time intensive. GPT-3 is, by contrast, quick, easy, and cheap to implement.',
              ),
              nlpText(
                '        Let\'s try a highly complex task, translation. As always, feel free to change the provided input to experiment.',
              ),
              nlpCard(context, send3Controller, receive3Controller),
              nlpText(
                '        It\'s possible you like your result! Especially if it\'s short. However, make it more complex by writing a longer excerpt, and both the accuracy and amount of garbage received back makes it entirely unusable. Google Translate, by comparison, uses NLP technology designed from the ground-up for its purpose of translation. Though, as generalized language models like GPT-3 see greater improvements, this will become increasingly possible.\n'
                '        Let\'s present another issue. Say you accidentally stumble into a pattern where the model feels it can provide a highly specialized response. The model may decide a unique is example is more relevant than the generalized one you were expecting. Below is an example found by Samuel Hammond, Director of Poverty and Welfare Policy at the Niskanen Center. Bold text is provided by Hammond, and the rest by GPT-3.',
              ),
              const Image(
                image: AssetImage('assets/img/ccp.png'),
                fit: BoxFit.fitWidth,
              ),
              nlpText(
                '        This isn\'t actually a topic \'banned\' by OpenAI for GPT-3 to discuss. Instead, it is picking up on transcripts from previous "AI assistant" available on the internet and duplicating that learned information. The language model does not understand what may be correct or appropriate responses for a question, it only understands the way that question has been answered before in its resources. This isn\'t because it is looking up questions, but because its understanding of language has been fundamentally changed by, in this case, a large amount of censored content about China.\n'
                '        One more issue. If you are unskilled at the language you are translating from, how might this affect the output? If the model picks up on your errors as a pattern, even very slight, it will produce output in the other language that mimics these errors. How about more subtle aspects, like your writing style itself? It\'s hard to gauge the output accuracy when you can\'t judge it yourself. Automation at scale, extrapolates these problems at scale.',
              ),
              nlpText('Garbage In: Garbage Out', heading: true),
              nlpText(
                '        We now have a sense of what can go wrong when generating responses. Data being used to train NLP, as well as many other deep-learning derivatives, is imperfect. As a result, the output is nothing less than imperfect as well. Additionally, it can be difficult to understand your own intent as understood by a language model, especially with the diversity of inputs that a released product will receive.\n'
                '        OpenAI argued that the immediate release of GPT-3\'s source code, as they did with previous works, would present serious negative, ethical implications on a world unprepared for it. Years later in November 2021, OpenAI published GPT-3 to the whole public, with no public visibility or access to the model, instead under a pay-per-use API business model. The API lowers the barrier to entry, but no visibility into the model itself, as with both GPT-1 and GPT-2, simultaneous to restructuring from a nonprofit into a for-profit company raises certain concern over the actual openness of OpenAI moving forward.\n'
                '        Between first teasing GPT-3 and now, OpenAI created an experimental filter to attempt to compensate for certain risks. Below is their list of content that is censored by the filter.',
              ),
              const Image(
                image: AssetImage('assets/img/gpt3-aup.png'),
                fit: BoxFit.fitHeight,
              ),
              nlpText(
                '        OpenAI is currently the leading provider of commercially available generalized NLP models, but they aren\'t the only ones. A research subsidiary of Alphabet, DeepMind, is also doing work in this space (though is at least, not yet, commercial). Less than two weeks ago, DeepMind pre-published an announcement having created a model that is, in some areas many-times over, more functional than GPT-3. At the same time, they published a full technical paper surveying previous research on the harms of NLP.\n'
                '        The following is an excerpt from the paper\'s abstract on identified risks, preserved for completeness:',
              ),
              const Image(
                image: AssetImage('assets/img/deepmind-aup.png'),
                fit: BoxFit.fitWidth,
              ),
              nlpText(
                '"\n'
                'The first risk area discusses fairness and toxicity risks in large-scale language models. This includes four distinct risks: LMs can create unfair discrimination and representational and material harm by perpetuating stereotypes and social biases, i.e. harmful associations of specific traits with social identities. Social norms and categories can exclude or marginalise those who exist outside them. Where a LM perpetuates such norms - e.g. that people called “Max” are “male”, or that “families” always consist of a father, mother and child - such narrow category use can deny or burden identities who differ. Toxic language can incite hate or violence or cause offense. Finally, a LM that performs more poorly for some social groups than others can create harm for disadvantaged groups, for example where such models underpin technologies that affect these groups. These risks stem in large part from choosing training corpora that include harmful language and overrepresent some social identities.\n'
                '        The second risk area includes risks from private data leaks or from LMs correctly inferring private or other sensitive information. These risks stem from private data that is present in the training corpus and from advanced inference capabilities of LMs.\n'
                '        The third risk area comprises risks associated with LMs providing false or misleading information. This includes the risk of creating less well-informed users and of eroding trust in shared information. Misinformation can cause harm in sensitive domains, such as bad legal or medical advice. Poor or false information may also lead users to perform unethical or illegal actions that they would otherwise not have performed. Misinformation risks stem in part from the processes by which LMs learn to represent language: the underlying statistical methods are not well-positioned to distinguish between factually correct and incorrect information.\n'
                '        The fourth risk area spans risks of users or product developers who try to use LMs to cause harm. This includes using LMs to increase the efficacy of disinformation campaigns, to create personalised scams or fraud at scale, or to develop computer code for viruses or weapon systems.\n'
                '        The fifth risk area focuses on risks from the specific use case of a “conversational agent” that directly interacts with human users. This includes risks from presenting the system as “human-like”, possibly leading users to overestimate its capabilities and use it in unsafe ways. Another risk is that conversation with such agents may create new avenues to manipulate or extract private information from users. LM-based conversational agents may pose risks that are already known from voice assistants, such as perpetuating stereotypes by self-presenting e.g. as “female assistant”. These risks stem in part from LM training objectives underlying such conversational agents and from product design decisions.\n'
                '        The sixth risk area includes risks that apply to LMs and Artificial Intelligence (AI) systems more broadly. Training and operating LMs can incur high environmental costs. LM-based applications may benefit some groups more than others and the LMs themselves are inaccessible to many. Lastly, LM-based automation may affect the quality of some jobs and undermine parts of the creative economy. These risks manifest particularly as LMs are widely used in the economy and benefits and risks from LMs are globally unevenly distributed.\n'
                '"\n'
                '        Below, you are welcome to test these harmful topics for yourself (but please be prepared for harmful results). The experimental GPT-3 filter is disabled; however, some content may still be censored. You can surely give it a bigger push than we have with the provided \'Send\' field below. Note though, however, that you are only increasing the probability of harmful content. Harmful content is possible in any provided context.',
              ),
              nlpCard(context, send4Controller, receive4Controller),
              //nlpCard(context, send6Controller, receive6Controller),
              //'        While the benefits of NLP are clear, there are some negative ethical and social risks that come with using the technology. '
              //'The first is discrimination in language processing models that result from NLPs accurately reflecting natural speech, including unjust and toxic tendencies which are present or can be inferred from the training data. '
              //'This can result in the misrepresentation of marginalized groups in terms of the function of the NLP, as well as the propagation of stereotypes online. The training data in question can be affected in two ways. '
              //'Firstly, the training data could be collected in such a context in which stereotypes are the existing state of affairs, which is possible due to the wide array of bigoted speech online. '
              //'As a result, an NLP which carries out it function correctly in terms of imitating human speech could amplify bigoted speech in the training data, a process referred to as ‘bias amplification’. '
              //'Secondly, it could be that data from marginalized groups is underrepresented in the training data. This leads to NLPs which fail to represent the language and recognize those from marginalized and excluded groups.'
              nlpText('In Practice', heading: true),
              nlpText(
                '        If you think these issues sound only recent, or even futuristic, you are unfortunately mistaken. While the rollout of highly proficient generalized NLP models is rather recent, the previously stated specialized models experience almost all of the same issues. In the vain of extracting maximum monetary gain, NLP models have been used for years now in, primarily, the automation of censorship.\n'
                '        There are two major facets to understand when it comes to how NLPs relate to the concept of censorship. The first is how, like with the discrimination problem, the content as well as the manipulation of training data factors into the NLP itself. For example, one study reported that Baidu Baike, a Chinese online encyclopedia has been crafted in such a way that there are “different associations between adjectives and a range of concepts” including “democracy, freedom, collective action, equality” as well as a range of Chinese historical figures and events instead of the Chinese version of Wikipedia, which is repeatedly blocked in the country. As a result, there are downstream implications in terms of the credibility of information being disseminated to the public by NLPs using this type of government censored information as training data.In one study, a language processor was found to exhibit “anti-Muslim and, to a lesser degree, antisemitic bias,” with the word Muslim being associated with “‘terrorist’ in 23% of test cases” while the word “Jewish was mapped to ‘money’ in 5% of all test cases” of the model. In the same model, female-sounding names were associated with generated stories about family and physical appearance and were often represented as less powerful than male-sounding names. Similar findings of the discrimination of marginalized groups have been found with other NLPs, emphasizing the social danger of the technology without serious changes into the quality and diversity of the data being utilized as training data.\n'
                '        YouTube, the most popular website globally after Google Search, is one such example. YouTube employs NLPs to moderate their platform. Certain words used in videos can trigger demonetization through layers of algorithms. There are algorithms for visual content, but for NLP queries, a separate deep-learning model creates a transcript of the video based on its audio content. This transcript can then be run through NLPs to determine \'content friendliness\'. YouTube trains their models primarily using crowd-sourced labor out of countries where homosexuality is illegal, or even punishable by death. These are also areas that YouTube wishes to expand its business into. Because of this, it should be less surprising that terms related to LGBTQIA+ issues (among many others) trigger demonetization. Every word it processes, through extensive training, is assigned a set of weights corresponding to every other word it could be in context to, and the \'advertiser friendliness\' of each connection. Based on complex calculations analyzing the relationships between these connections, final conclusions are made about allowing monetization or not for a video.\n'
                '        Using the word \'gay\' or \'lesbian\' within the first 10 seconds of the video resulted in the video being demonetized in every scenario only a couple of years ago. The mapping of words to words inside of the NLP model, as reinforced by the crowd-labor, automates this against the entire platform. Not only does demonetization prevent content creators from earning money through ads on their content, but since YouTube no longer has a financial incentive to promote the content through separate recommendation algorithms, they have no need to make the content easily visible—in search results, in recommendation feeds, and even in subscription feeds. Given the near-monopoly YouTube has on publishing video content, this censors the creators and their content almost entirely and actively represses minorities, among censoring discussions of certain historical events, many other categories, or simply videos demonetized in error. This has been going on for years as of writing. Major improvements to the NLP, especially regarding LGBTQIA+ content, has been made since, but only after years of it ruining careers, blocking related content\'s distribution, multiple studies, multiple legal cases, and unionization threats. A study by BeurlingAI even went as far as to test 15,000 words, in separate videos, against YouTube’s algorithms to compile a list of acceptable, sometimes acceptable, and unacceptable words in regards to monetization, which are still available.\n'
                '        Beyond YouTube, NLP opens up greater opportunities for censorship than ever before. Censorship taking place at the ISP or even domain registrar level, as with the cases involving the company XYZ.com, can take place at a much wider, automated scale. In that XYZ.com case, websites were banned on web addresses, but it would be easier than ever to gauge actual sentiment behind statements, versus banning specific words, with the use of NLP technologies, as in the case of YouTube.',
              ),
              nlpText('Conclusion', heading: true),
              nlpText(
                  //'        In CPSC 183, we saw the consumer impact of how bad data fed to HP computer’s facial recognition feature led to it failing to recognize the faces of Black people, and therefore completely failing to serve its function. \n'
                  '        For NLPs to be successful and equal for every user, training data must reflect the culture and diversity of its users. Not as a result of their proportion in general datasets, which further suppresses the minority, but as mandatory, tested for items. Knowledge of NLP technology and its weaknesses needs to be known widespread by, crucially, its developers, but users too. An understanding must also be made on the limitations of using such technology for the enforcement of law, and \'heap-like\' problems.\n'
                  '        Many of these issues have already existed for years now, and full implications are unknown. The accuracy of generalized NLP models is improving at a rapid rate, against all expectations of slowing down. Maintaining wide awareness of NLP technology, as with other harmful technology, and preparing consumer protection law against them, will be critical to a healthier relationship with technology moving forward.\n'),
              nlpCard(context, send5Controller, receive5Controller),
              const Divider(
                height: 40,
                thickness: 2,
                indent: 10,
                endIndent: 0,
                color: Colors.grey,
              ),
              nlpText('Authors', heading: true),
              nlpText(
                'Cody Neiman <neiman@cody.to>\n'
                'Angela Avonce <angela.avonce@yale.edu>\n'
                'Kaise Dualeh <kaise.dualeh@yale.edu>\n\n'
                'Source Code: TBA\n',
              ),
              nlpText('Sources', heading: true),
              nlpText(
                  'Samuel Hammond Finding:\nhttps://nitter.hu/hamandcheese/status/1430205176233840640\n\n'
                  'OpenAI Experimental Filter:\nhttps://openai.com/blog/api-no-waitlist/\n\n'
                  'Ethical and social risks of harm from Language Models:\nhttps://deepmind.com/research/publications/2021/ethical-and-social-risks-of-harm-from-language-models\n\n'
                  'Censorship of Online Encyclopedias: Implications for NLP Models:\nhttps://dl.acm.org/doi/10.1145/3442188.3445916\n\n'
                  'Beurling AI - YouTube Demonetization Report:\nhttps://docs.google.com/document/d/18B-X77K72PUCNIV3tGonzeNKNkegFLWuLxQ_evhF3AY/edit\n\n'
                  'XYZ.com Chinese Censorship:\nhttps://www.wsj.com/articles/china-censors-your-internet-1446416123',
                  fsize: 14),
            ],
          ),
        ),
      ),
    );
  }

  Padding nlpText(String text, {bool heading = false, double fsize = 18}) {
    FontWeight style;
    if (heading) {
      style = FontWeight.bold;
    } else {
      style = FontWeight.normal;
    }

    return Padding(
      padding: const EdgeInsets.only(top: 5),
      child: RichText(
        text: TextSpan(
          text: text,
          style: GoogleFonts.lora(
            textStyle: TextStyle(
                color: Colors.black, fontSize: fsize, fontWeight: style),
          ),
        ),
      ),
    );
  }

  Card nlpCard(
    BuildContext context,
    TextEditingController sendController,
    TextEditingController receiveController,
  ) {
    return Card(
      child: Container(
        decoration:
            BoxDecoration(border: Border.all(width: 1, color: primaryColor)),
        height: 600,
        //color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Flexible(
                flex: 1,
                child: Text(
                  'Send',
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
              Flexible(
                flex: 5,
                child: TextField(
                  controller: sendController,
                  keyboardType: TextInputType.multiline,
                  minLines: 5,
                  maxLines: 5,
                ),
              ),
              Flexible(
                flex: 1,
                child: Text(
                  'Receive',
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
              Flexible(
                flex: 8,
                child: TextField(
                  controller: receiveController,
                  keyboardType: TextInputType.multiline,
                  minLines: 10,
                  maxLines: 10,
                ),
              ),
              Flexible(
                flex: 2,
                child: ElevatedButton(
                  style: ButtonStyle(
                    fixedSize: MaterialStateProperty.all(
                      const Size(110, 40),
                    ),
                    backgroundColor: MaterialStateProperty.all(Colors.black26),
                  ),
                  child: RichText(
                    text: const TextSpan(
                      children: [
                        TextSpan(
                            text: "Run ",
                            style:
                                TextStyle(fontSize: 22, color: Colors.white)),
                        WidgetSpan(
                          child: Icon(
                            Icons.auto_awesome_outlined,
                            size: 22,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  onPressed: () =>
                      _sendRequest(sendController, receiveController),
                ),
              )
            ],
          ),
        ),
      ),
      elevation: 8,
      margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
    );
  }
}
